<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form (){
        return view ('register');
    }

    public function welcome (Request $request){
        //dd($request->all());
        $nama_dpn = $request["fname"];
        $nama_blkng = $request["lname"];
        return view ('welcome',[
            'nama_dpn' => $nama_dpn,
            'nama_blkng' => $nama_blkng
            ]);
    }
}
