<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <link rel="shortcut icon" href="Logo Fav.png">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h2>Sign Up Form</h2>

    <form id="bio" action="/welcome" method="post">
        @csrf
        <label for="fname">First Name : </label><br><br>
        <input type="text" id="fname" name="fname"><br><br><br>

        <label for="lname">Last Name : </label><br><br>
        <input type="text" id="lname" name="lname"><br><br><br>

        <label for="gender">Gender :</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="male" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="others" name="gender" value="others">
        <label for="others">Others</label><br><br><br>

        <label for="natioanally">Nationally : </label><br><br>
        <select id="nationally" name="nationally">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singapore</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Australia</option>
        </select><br><br><br>

        <label for="language">Language Spoken :</label><br><br>

        <input type="checkbox" id="language1" name="language1" value="BahasaIndonesia">
        <label for="language1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2">English</label><br>
        <input type="checkbox" id="language3" name="language3" value="Others">
        <label for="language3">Others</label>

        <br><br><br>

        <label for="bio">Bio :</label><br>
        <textarea name="bio" form="bio" cols="30" rows="10"></textarea>
        <br><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>